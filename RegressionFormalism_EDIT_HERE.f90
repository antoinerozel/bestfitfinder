  ! Equations for the regression must be written here
  ! the variable to be found has to be called "regression"
  ! an example is:
  ! regression=a0+a1*x(i)
  !
  ! - the user can write as many lines as required here and can use several
  ! substeps to build a clean algorithm
  ! - all parameters coming from the original matrix must be called with "(i)"
  ! - additional parameters can be declared in AdditionalFortranDeclarations_EDIT_HERE.f90

  !regression=a0*y(i)**a1
  regression=a0+a1*x(i)
  

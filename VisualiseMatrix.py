#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import sys, getopt
import math

#Here define your columns
#cU cL ft fb Ys ze As Detas Vs Etas Af Detaf Vf Etaf
#0  1  2  3  4  5  6  7     8  9    10 11    12 13


#List of names of each column. Used in plots ----------------
ColumnList = ["chi UM","chi LM","f top","f bot","Yield stress","Zeta","A disl","Delta Eta disl","V disl","EtaRef disl","A diff","Delta Eta diff","V diff","EtaRef diff"]

#choice of which columns are shown in various ways ----------
yaxisindex=11       #what will be plotted on y axis
xaxisindex=2       #what will be plotted on x axis
pointsizeindex=0   #the size of each point will depend on this column
colorindex=3       #the color of each point will depend on this column

#log scale or not? (assumes log scale for now) --------------
xaxislog=1
yaxislog=1
pointsizelog=1
colorlog=1

#min and max sizes of the points displayed ------------------
PointSizeMinValue=1
PointSizeMaxValue=80

# the axis are sometimes poorly detected, this forces the axis to be around data range
AxisOffset=0.1


# the main function
def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print ('TYPE: python VisualiseMatrix.py -i <inputfile> -o <outputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('TYPE: python VisualiseMatrix.py -i <inputfile> -o <outputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg

   # reads the file
   f = open(inputfile, 'r')
   lines = f.readlines()
   f.close

   # initialize some variable to be lists:
   x1 = []  # x coordinate
   y1 = []  # y coordinate
   a1 = []  # area of the points
   c1 = []  # color of the points

   # scan the rows of the file stored in lines, and put the values into some variables:
   for line in lines:
       p = line.split()
       x1.append(float(p[xaxisindex]))
       y1.append(float(p[yaxisindex]))
       #areas and colors use log scale here
       a1.append(math.log10(float(p[pointsizeindex])))
       c1.append(math.log10(float(p[colorindex])))
       xv = np.array(x1)
       yv = np.array(y1)
       av = np.array(a1)
       cv = np.array(c1)

   #now rescales the point size according to the choice of the user
   minav=min(av) # already log scale
   maxav=max(av)
   meanav=(minav+maxav)/2
   av2=PointSizeMinValue + (av-minav)/(maxav-minav)*(PointSizeMaxValue-PointSizeMinValue)


   #rescales the axis------------
   Dx=math.log10(max(xv))-math.log10(min(xv))
   xmin=math.pow(10,math.log10(min(xv))-AxisOffset*Dx)
   xmax=math.pow(10,math.log10(max(xv))+AxisOffset*Dx)
   Dy=math.log10(max(yv))-math.log10(min(yv))
   ymin=math.pow(10,math.log10(min(yv))-AxisOffset*Dy)
   ymax=math.pow(10,math.log10(max(yv))+AxisOffset*Dy)


   # now, plot the data:
   plt.scatter(xv,yv,c=cv,s=av2,alpha=0.5)

   plt.xlim(xmin,xmax)
   plt.ylim(ymin,ymax)
   plt.xscale('log')
   plt.yscale('log')
   plt.xlabel(ColumnList[xaxisindex])
   plt.ylabel(ColumnList[yaxisindex])

   PointSizeMeanValue = (PointSizeMinValue+PointSizeMaxValue)/2
   l1 = plt.scatter([],[], s=PointSizeMinValue,  c="black", edgecolors='none')
   l2 = plt.scatter([],[], s=PointSizeMeanValue, c="black", edgecolors='none')
   l3 = plt.scatter([],[], s=PointSizeMaxValue,  c="black", edgecolors='none')

   index_cvmin=np.argmin(cv)
   index_cvmax=np.argmax(cv)

   l4 = plt.scatter([],[], s=PointSizeMeanValue, c="red", edgecolors='none')
   l5 = plt.scatter([],[], s=PointSizeMeanValue, c="red", edgecolors='none')
   l6 = plt.scatter([],[], s=PointSizeMeanValue, c="red", edgecolors='none')

   labels = [ str(pow(10,minav)) , str(pow(10,meanav)) , str(pow(10,maxav)) ]

   # to specify more stuff
   #leg = plt.legend([l1, l2, l3], labels, ncol=1, fontsize=12,
   #handlelength=2, loc = 8, borderpad = 1.8,
   #handletextpad=1, title=ColumnList[pointsizeindex], scatterpoints = 1)
   
   # second legend (has to be defined first)
   leg2 = plt.legend([l4, l5, l6], labels, ncol=1,title=ColumnList[pointsizeindex],loc=1)

   # first legend (point sizes)
   #plt.legend([l1, l2, l3], labels, ncol=1,title=ColumnList[pointsizeindex])
   plt.legend([l1, l2, l3], labels, ncol=1,title=ColumnList[pointsizeindex],loc=4)
   
   plt.gca().add_artist(leg2)
   
   plt.show()





# do not delete
if __name__ == "__main__":
   main(sys.argv[1:])
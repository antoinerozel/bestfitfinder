#!/bin/bash

rm PLOTRES 2> /dev/null

# parameters that should be equated
for param in ra Ti l m Fe Mg DT
do
    if [ $param = ra ]; then
	nval=3
	value[1]=0.5
	value[2]=1
	value[3]=2
    fi
    if [ $param = Ti ]; then
	nval=3
	value[1]=1600
	value[2]=1800
	value[3]=2000
    fi
    if [ $param = l ]; then
	nval=2
	value[1]=50
	value[2]=100
    fi
    if [ $param = m ]; then
	nval=9
	value[1]=1
	value[2]=1.5
	value[3]=2
	value[4]=2.5
	value[5]=3
	value[6]=4
	value[7]=5
	value[8]=6
	value[9]=7
    fi
    if [ $param = Fe ]; then
	nval=3
	value[1]=0.5
	value[2]=1
	value[3]=1.5
    fi
    if [ $param = Mg ]; then
	nval=3
	value[1]=0.5
	value[2]=1
	value[3]=1.5
    fi
    if [ $param = DT ]; then
	nval=1
	value[1]=0
    fi
    for i in `seq 1 $nval`
    do
	echo "!__PLOT_RESTRICTION__open(unit=11,file=\"Outputs/"$param"_"${value[$i]}"\",position=\"append\")" >> PLOTRES
	echo "!__PLOT_RESTRICTION__if("$param"(i).eq."${value[$i]}")then" >> PLOTRES
	echo "!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression" >> PLOTRES
	echo "!__PLOT_RESTRICTION__endif" >> PLOTRES
	echo "!__PLOT_RESTRICTION__close(11)" >> PLOTRES
    done
done

# parameters that should be lower than
for param in Fem
do
    if [ $param = Fem ]; then
	nval=1
	value[1]=0.1
    fi
    for i in `seq 1 $nval`
    do
	echo "!__PLOT_RESTRICTION__open(unit=11,file=\"Outputs/"$param"_lowerthan"${value[$i]}"\",position=\"append\")" >> PLOTRES
	echo "!__PLOT_RESTRICTION__if("$param"(i).lt."${value[$i]}")then" >> PLOTRES
	echo "!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression" >> PLOTRES
	echo "!__PLOT_RESTRICTION__endif" >> PLOTRES
	echo "!__PLOT_RESTRICTION__close(11)" >> PLOTRES
    done
done


# parameters that should be greater than
for param in DT Fem
do
    if [ $param = DT ]; then
	nval=1
	value[1]=0.0
    fi
    if [ $param = Fem ]; then
	nval=1
	value[1]=0.1
    fi
    for i in `seq 1 $nval`
    do
	echo "!__PLOT_RESTRICTION__open(unit=11,file=\"Outputs/"$param"_greaterthan"${value[$i]}"\",position=\"append\")" >> PLOTRES
	echo "!__PLOT_RESTRICTION__if("$param"(i).gt."${value[$i]}")then" >> PLOTRES
	echo "!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression" >> PLOTRES
	echo "!__PLOT_RESTRICTION__endif" >> PLOTRES
	echo "!__PLOT_RESTRICTION__close(11)" >> PLOTRES
    done
done


#!/bin/bash
# This program fills in and compiles a prepared fortran code (bestfitfinder.sed.f90)
# fill in the equation for a fit
mv RESULT_chi_BestParams.dat PreviousBestParameters
rm RESULT_* Outputs/*

# Syste type
linux=0 # 0 for mac, 1 for linux

# The name of the fortran compiler you want to use
FortranCompiler=gfortran

# If you want special files to be plotted, put that to 1
# (open the file bestfitfinder.sed.f90 and find the part with !__PLOT_RESTRICTION__)
plot_restriction=0

# The name of the matrix file
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_30"
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_100"
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_300"
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_1000"
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_3000"
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_10000"
#matrixfilename="BENCHMARK_MATRIX_RESOLUTION_30000"
matrixfilename="ExampleMatrix"
#matrixfilename="Nu_Ra"

#Name each column of your input file
param=(x y)

# parameters that will be used to visualise the quality of the regression
paramvisu=(coZ)
inx=Ra

# these are reference values used for the plots at the end of the regression
# use Z at the end of each name (to make it as short as possible)
# use the same name!!
xZ=1.
yZ=1.

# of course, param2fit must be part of the matrix
# write the whole fortran expression with (i) after each parameter
# example: param2fit="(1.0-Ti(i))"
param2fit="y(i)"
#param2fit="x(i)"

# If the parameter to fit contains zeros, make this 1, otherwise 0, 2 does not renormalise misfit
Stuff2FitContainsZEROS=2

forcefit=0
#forcefit=1 # just to try with no forced parameters

# Number of steps in each dimension of the parameter space (20 should be fine)
maxsteps=15

# Number of resolutions to try (5 should be fine in most cases, more is better)
ires=7

# 1 will rerun the code using initial guess close to previous solution
refine=0
boundaries_away_percent=0.2 #5 # fixes the boundaries of the search around the previous best

#standard deviation? Bugged...
compute_SD=1
showDetailsSD=0 # to show details of standard deviation calculation (somtimes SD is not found)

#Intermediate variables you might need, declare in this file
AdditionalDeclarationFile=AdditionalFortranDeclarations_EDIT_HERE.f90

#Edit this file to write down the regression formalism
EquationFile=RegressionFormalism_EDIT_HERE.f90


# Initial boundaries of each parameter ---------------------------------------------

amin[0]=-1.
amax[0]=1.
amin[1]=-1.
amax[1]=1.
amin[2]=-1.
amax[2]=1.
amin[3]=-1.
amax[3]=1.
amin[4]=-1.
amax[4]=1.
amin[5]=-1.
amax[5]=1.
amin[6]=-1.
amax[6]=1.
amin[7]=-1.
amax[7]=1.
amin[8]=-1.
amax[8]=1.
amin[9]=-1.
amax[9]=1.
amin[10]=-1.
amax[10]=1.
amin[11]=-1.
amax[11]=1.

#-----------------------------------------------------------------------
#-------------------- NOT TO BE TOUCHED AREA ---------------------------
#-----------------------------------------------------------------------
#-----------------------------------------------------------------------

SourcesFolder=OtherSources


number_of_parameters=0
nonzero=1
while [ $nonzero -eq 1 ]; do
    nonzero=`cat RegressionFormalism_EDIT_HERE.f90|grep -v '!'|grep a$number_of_parameters|wc|gawk '{print $1}'`
    if [ $nonzero -eq 1 ]; then
	number_of_parameters=$(( $number_of_parameters + 1 ))
    fi
done
echo Number of parameters detected: $number_of_parameters

#if refine = 1  it means the code is rerun using the last solution
#amins and amaxs are initialised around the past solutions
if [ $refine -eq 1 ]; then
    for i in `seq 1 $number_of_parameters`
    do
	amin[$(($i-1))]=`cat PreviousBestParameters|gawk -v i=$i -v d=$boundaries_away_percent '{if($(i+1)<0.){print $(i+1)*(1.+0.01*d)}else{print $(i+1)*(1.-0.01*d)}}'`
	amax[$(($i-1))]=`cat PreviousBestParameters|gawk -v i=$i -v d=$boundaries_away_percent '{if($(i+1)<0.){print $(i+1)*(1.-0.01*d)}else{print $(i+1)*(1.+0.01*d)}}'`

	echo initial param $(($i-1)): ${amin[$(($i-1))]}, ${amax[$(($i-1))]}

    done
fi

imax=`wc $matrixfilename|gawk '{ print $1 }'`
nwords=`wc $matrixfilename|gawk '{ print $2 }'`
ncol=$(($nwords/$imax))
readin=`echo ${param[*]}|sed 's@ @(i),@g'|gawk '{ print $1"(i)" }'`
if [ $showDetailsSD -eq 1 ]; then
    showDetailsSDstring=".true."
else
    showDetailsSDstring=".false."
fi
cat $SourcesFolder/bestfitfinder1.sed.f90 $AdditionalDeclarationFile \
    $SourcesFolder/bestfitfinder2.sed.f90 $EquationFile \
    $SourcesFolder/bestfitfinder3.sed.f90 $EquationFile \
    $SourcesFolder/bestfitfinder4.sed.f90 > $SourcesFolder/bestfitfinder.sed.f90

cp $SourcesFolder/bestfitfinder.sed.f90 $SourcesFolder/bestfitfinder_tmp.sed.f90
for i in $(seq 0 $(($ncol-1)))
do
    #
    if [ $linux -eq 1 ]; then
	string="s@!__0__@real,dimension(1:imax)::${param[i]}\n!__0__@g"  # On linux
    else
	string="s@!__0__@real,dimension(1:imax)::${param[i]}__TOREPLACEBYNEWLINE__!__0__@g" # On Mac
    fi
    #
    #string="s@real()@real2@g"
    if [ $linux -eq 1 ]; then
	sed $string $SourcesFolder/bestfitfinder_tmp.sed.f90 > $SourcesFolder/bestfitfinder_tmp2.sed.f90 # Linux
    else
	sed $string $SourcesFolder/bestfitfinder_tmp.sed.f90 |sed -e 's/__TOREPLACEBYNEWLINE__/\'$'\n/g' > $SourcesFolder/bestfitfinder_tmp2.sed.f90 # Mac
    fi
    mv $SourcesFolder/bestfitfinder_tmp2.sed.f90 $SourcesFolder/bestfitfinder_tmp.sed.f90
    #
done

for i in $(seq 0 $(($number_of_parameters-1)))
do
    #
    if [ $linux -eq 1 ]; then
	string="s@!__1__@real::a$i,a"$i"min,a"$i"max,a"$i"best,a"$i"step\n!__1__@g"
	stringSDD="s@!__SD_Declarations__@real::a"$i"_SD,a"$i"_SD_min,a"$i"_SD_max\n!__SD_Declarations__@g"
	stringShowDetailsSD="s@__showDetailsSD__@$showDetailsSDstring@g"
	stringmin="s@!__minmaxdeclaration__@a"$i"min=${amin[$i]}\na"$i"max=${amax[$i]}\n!__minmaxdeclaration__@g"
	stringb1="s@!__bloc1__@a"$i"step=(a"$i"max-a"$i"min)/maxstepsreal\n!__bloc1__@g"
	stringloopstart="s@!__loop_start__@do____i"$i"=0,maxsteps\na"$i"=a"$i"min+real(i"$i")*a"$i"step\n!__loop_start__@g"
	stringb2="s@!__bloc2__@a"$i"best=a"$i"\n!__bloc2__@g"
	stringblocbest="s@!__blocbest__@,a"$i"best!__blocbest__@g"
	stringloopclose="s@!__loop_closing__@enddo\n!__loop_closing__@g"
        #
	if [ $i -ne 0 ]; then
	    string3="s@!__bloc3__@else____if(a"$i"best.eq.a"$i"min)then\nwrite(*,*)\"Bad____boundary\"\na"$i"min=a"$i"min-(a"$i"max-a"$i"min)/2.0\nwrongbound=1\n!__bloc3__@g"
	    string4="s@!__bloc4__@else____if(a"$i"best.eq.a"$i"max)then\nwrite(*,*)\"Bad____boundary\"\na"$i"max=a"$i"max+(a"$i"max-a"$i"min)/2.0\nwrongbound=1\n!__bloc4__@g"
	else
	    string3="s@!__bloc3__@!__bloc3__@g"
	    string4="s@!__bloc4__@!__bloc4__@g"
	fi
	string5="s@!__bloc5__@a"$i"min=a"$i"best-1.5*a"$i"step\na"$i"max=a"$i"best+1.5*a"$i"step\n!__bloc5__@g"
	string6="s@!__bloc6__@a"$i"=a"$i"best\n!__bloc6__@g"
	string7="s@__np__@$number_of_parameters@g"
	string8="s@__ff__@$forcefit@g"
	sed $string $SourcesFolder/bestfitfinder_tmp.sed.f90 \
	    |sed $stringmin |sed $stringb1 \
	    |sed $stringSDD \
	    |sed $stringShowDetailsSD \
	    |sed $stringloopstart \
	    |sed $stringb2 \
	    |sed $stringblocbest \
	    |sed $stringloopclose \
	    |sed $string3 \
	    |sed $string4 \
	    |sed $string5 \
	    |sed $string6 \
	    |sed $string7 \
	    |sed $string8 \
	    |sed 's@____@ @g' \
	    > $SourcesFolder/bestfitfinder_tmp2.sed.f90
	mv $SourcesFolder/bestfitfinder_tmp2.sed.f90 $SourcesFolder/bestfitfinder_tmp.sed.f90
    else
	string="s@!__1__@real::a$i,a"$i"min,a"$i"max,a"$i"best,a"$i"step__TOREPLACEBYNEWLINE__!__1__@g"
	stringSDD="s@!__SD_Declarations__@real::a"$i"_SD,a"$i"_SD_min,a"$i"_SD_max__TOREPLACEBYNEWLINE__!__SD_Declarations__@g"
	stringShowDetailsSD="s@__showDetailsSD__@$showDetailsSDstring@g"
	if [ $plot_restriction -eq 1 ]; then
	    stringPLOT="s@!__PLOT_RESTRICTION__@@g"
	else
	    stringPLOT="s@kqwukuazvkuzvkvewdw@@g"
	fi
	stringmin="s@!__minmaxdeclaration__@a"$i"min=${amin[$i]}__TOREPLACEBYNEWLINE__a"$i"max=${amax[$i]}__TOREPLACEBYNEWLINE__!__minmaxdeclaration__@g"
	stringb1="s@!__bloc1__@a"$i"step=(a"$i"max-a"$i"min)/maxstepsreal__TOREPLACEBYNEWLINE__!__bloc1__@g"
	stringloopstart="s@!__loop_start__@do____i"$i"=0,maxsteps__TOREPLACEBYNEWLINE__a"$i"=a"$i"min+real(i"$i")*a"$i"step__TOREPLACEBYNEWLINE__!__loop_start__@g"
	stringb2="s@!__bloc2__@a"$i"best=a"$i"__TOREPLACEBYNEWLINE__!__bloc2__@g"
	stringblocbest="s@!__blocbest__@,a"$i"best!__blocbest__@g"
	stringloopclose="s@!__loop_closing__@enddo__TOREPLACEBYNEWLINE__!__loop_closing__@g"
        #
	if [ $i -ne 0 ]; then
	    string3="s@!__bloc3__@else____if(a"$i"best.eq.a"$i"min)then__TOREPLACEBYNEWLINE__write(*,*)\"Bad____boundary\"__TOREPLACEBYNEWLINE__a"$i"min=a"$i"min-(a"$i"max-a"$i"min)/2.0__TOREPLACEBYNEWLINE__wrongbound=1__TOREPLACEBYNEWLINE__!__bloc3__@g"
	    string4="s@!__bloc4__@else____if(a"$i"best.eq.a"$i"max)then__TOREPLACEBYNEWLINE__write(*,*)\"Bad____boundary\"__TOREPLACEBYNEWLINE__a"$i"max=a"$i"max+(a"$i"max-a"$i"min)/2.0__TOREPLACEBYNEWLINE__wrongbound=1__TOREPLACEBYNEWLINE__!__bloc4__@g"
	else
	    string3="s@!__bloc3__@!__bloc3__@g"
	    string4="s@!__bloc4__@!__bloc4__@g"
	fi
	string5="s@!__bloc5__@a"$i"min=a"$i"best-1.5*a"$i"step__TOREPLACEBYNEWLINE__a"$i"max=a"$i"best+1.5*a"$i"step__TOREPLACEBYNEWLINE__!__bloc5__@g"
	string6="s@!__bloc6__@a"$i"=a"$i"best__TOREPLACEBYNEWLINE__!__bloc6__@g"
	string7="s@__np__@$number_of_parameters@g"
	string8="s@__ff__@$forcefit@g"
	sed $string $SourcesFolder/bestfitfinder_tmp.sed.f90 \
	    |sed $stringSDD \
	    |sed $stringShowDetailsSD \
	    |sed $stringPLOT \
	    |sed $stringmin |sed $stringb1 \
	    |sed $stringloopstart \
	    |sed $stringb2 \
	    |sed $stringblocbest \
	    |sed $stringloopclose \
	    |sed $string3 \
	    |sed $string4 \
	    |sed $string5 \
	    |sed $string6 \
	    |sed $string7 \
	    |sed $string8 \
	    |sed 's@____@ @g' \
	    |sed -e 's/__TOREPLACEBYNEWLINE__/\'$'\n/g'\
	    > $SourcesFolder/bestfitfinder_tmp2.sed.f90
    mv $SourcesFolder/bestfitfinder_tmp2.sed.f90 $SourcesFolder/bestfitfinder_tmp.sed.f90
    fi
    #
done

st1=s@__imax__@$imax@g
st2=s@__matrixfilename__@$matrixfilename@g
st3=s@__readin__@$readin@g
st4=s@__param2fit__@$param2fit@g
st5=s@__maxsteps__@$maxsteps@g
st6=s@__ires__@$ires@g
st7=s@__equation__@$equation@g
st8=s@__Stuff2FitContainsZEROS__@$Stuff2FitContainsZEROS@g
st9=s@__equation2__@$equation2@g
st10=s@__preEquation1__@$preequation1@g
st11=s@__preEquation2__@$preequation2@g
st12=s@__preEquation3__@$preequation3@g
st13=s@__preEquation4__@$preequation4@g
st14=s@__preEquation5__@$preequation5@g
st17=s@__equation3__@$equation3@g
st18=s@__equation4__@$equation4@g
st19=s@__equation5__@$equation5@g
st20=s@__equation6__@$equation6@g
st21=s@__equation7__@$equation7@g

sed $st1 $SourcesFolder/bestfitfinder_tmp.sed.f90 \
    |sed $st2 |sed $st3 \
    |sed $st4 |sed $st5 \
    |sed $st6 |sed $st7 \
    |sed $st8 |sed $st9 \
    |sed $st10 |sed $st11 \
    |sed $st12 |sed $st13 \
    |sed $st14 |sed $st17 \
    |sed $st18 |sed $st19 \
    |sed $st20 |sed $st21 > $SourcesFolder/tmp

#adding standard deviation blocs
if [ $compute_SD -eq 1 ]; then
    for i in $(seq 0 $(($number_of_parameters-1)))
    do
	cat $EquationFile |sed "s@a$i@a"$i"_SD@g" > $SourcesFolder/TMPFILES/SDbloc$i
	cat $SourcesFolder/tmp $SourcesFolder/SD_Bloc1A \
	    $SourcesFolder/TMPFILES/SDbloc$i \
	    $SourcesFolder/SD_Bloc1B \
	    $SourcesFolder/TMPFILES/SDbloc$i \
	    $SourcesFolder/SD_Bloc1C \
	    $SourcesFolder/TMPFILES/SDbloc$i \
	    $SourcesFolder/SD_Bloc2 > $SourcesFolder/bestfitfinder_tmp.sed.f90
	string="s@__param__@a$i@g"
	sed $string $SourcesFolder/bestfitfinder_tmp.sed.f90 > $SourcesFolder/tmp
    done
fi
cat $SourcesFolder/tmp $SourcesFolder/End.f90 > $SourcesFolder/bestfitfinder.f90
for i in $(seq 0 $(($number_of_parameters-1)))
do
    stringblocfinalSD="s@!__blocFinalSD__@,a"$i"_SD!__blocFinalSD__@g"
    
    sed $stringblocfinalSD $SourcesFolder/bestfitfinder.f90 \
	> $SourcesFolder/tmp
    mv $SourcesFolder/tmp $SourcesFolder/bestfitfinder.f90
    
done


cat $SourcesFolder/bestfitfinder.f90| sed -e $'s/C15(i),/C15(i),\&\\\n/' > $SourcesFolder/bestfitfinderLineCut.f90

rm FindCoefNoLin 2>/dev/null

$FortranCompiler -o FindCoefNoLin $SourcesFolder/bestfitfinderLineCut.f90

./FindCoefNoLin

exit

# this part is now commented because it does not work anymore
# before this was used
#equation="regression=a0+a1*x(i)"
#equation2=""

for n in `seq 1 $number_of_parameters`
do
    a[$n]=`cat fort.118|gawk -v n=$(($n+1)) '{print $n}'`
    eqvisu=`echo $equation|sed "s@a"$(($n-1))"@("${a[$n]}")@g"`
    equation=$eqvisu
    eqvisu2=`echo $equation2|sed "s@a"$(($n-1))"@("${a[$n]}")@g"`
    equation2=$eqvisu2
done
eqvisu=`echo $equation|sed 's@(i)@@g'`
eqvisu2=`echo $equation2|sed 's@(i)@@g'`
str1=s@__eqvisu__@$eqvisu@g
str2=s@__eqvisu2__@$eqvisu2@g
#sed $str1 represent_ref_case.sed.f90 | sed $str2 > represent_ref_case.f90
#gfortran -o run represent_ref_case.f90
#./run

for i in `echo ${paramvisu[*]}`
do
    j=`echo $i|sed 's@Z@@'`
    eval k="\$$i"
    eqtmp=`echo $equation|sed "s@"$j"(i)@("$k"-"$j"(i))@g"`
    equation=$eqtmp
    eqtmp2=`echo $equation2|sed "s@"$j"(i)@("$k"-"$j"(i))@g"`
    equation2=$eqtmp2
done

for n in `seq 1 $number_of_parameters`
do
    a[$n]=`cat fort.118|gawk -v n=$(($n+1)) '{print $n}'`
    eqvisu=`echo $equation|sed "s@a"$(($n-1))"@("${a[$n]}")@g"`
    equation=$eqvisu
    eqvisu2=`echo $equation2|sed "s@a"$(($n-1))"@("${a[$n]}")@g"`
    equation2=$eqvisu2
done

imax=`wc $matrixfilename|gawk '{ print $1 }'`
nwords=`wc $matrixfilename|gawk '{ print $2 }'`
ncol=$(($nwords/$imax))
readin=`echo ${param[*]}|sed 's@ @(i),@g'|gawk '{ print $1"(i)" }'`

cp $SourcesFolder/ModifiedResultPlot.sed.f90 $SourcesFolder/ModifiedResultPlot_tmp.sed.f90
for i in $(seq 0 $(($ncol-1)))
do
#    if [ $linux -eq 1 ]; then
#	string="s@!__0__@real,dimension(1:imax)::${param[i]}\n!__0__@g"  # On linux
#	sed $string ModifiedResultPlot_tmp.sed.f90 \
#	    > ModifiedResultPlot_tmp2.sed.f90 # Linux
#    else
	string="s@!__0__@real,dimension(1:imax)::${param[i]}__TOREPLACEBYNEWLINE__!__0__@g" # On Mac
	sed $string $SourcesFolder/ModifiedResultPlot_tmp.sed.f90 \
	    |sed -e 's/__TOREPLACEBYNEWLINE__/\'$'\n/g' \
	    > $SourcesFolder/ModifiedResultPlot_tmp2.sed.f90 # Mac
    #fi
    mv $SourcesFolder/ModifiedResultPlot_tmp2.sed.f90 $SourcesFolder/ModifiedResultPlot_tmp.sed.f90
done

st1=s@__imax__@$imax@g
st2=s@__matrixfilename__@$matrixfilename@g
st3=s@__readin__@$readin@g
st4=s@__param2fit__@$param2fit@g
st5=s@__eq1__@$equation@g
st6=s@__eq2__@$equation2@g
st7=s@__inx__@$inx@g

sed $st1 $SourcesFolder/ModifiedResultPlot_tmp.sed.f90 \
    |sed $st2 |sed $st3 \
    |sed $st4 |sed $st5 \
    |sed $st6 |sed $st7 \
    > $SourcesFolder/ModifiedResultPlot.f90
$FortranCompiler -o run $SourcesFolder/ModifiedResultPlot.f90
./run

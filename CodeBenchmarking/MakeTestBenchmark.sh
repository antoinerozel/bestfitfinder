#!/bin/bash
# this script generates a matrix to be used by the main code to benchmark it
# x between 0 and 2
# solution of the equation is
# y = 2.46 - 3.65*x**1.53 (+ random noise)

#noise=10.
#noise=3.
noise=0.
for res in 30 100 300 1000 3000 10000 30000
do
    str1=s/__n__/$res/g
    str2=s/__noise__/$noise/g
    sed $str1 MakeTestBenchmark.sed.f90 |sed $str2 > MakeTestBenchmark.f90
    gfortran -o makebenchmark MakeTestBenchmark.f90
    ./makebenchmark > BENCHMARK_MATRIX_RESOLUTION_$res
done

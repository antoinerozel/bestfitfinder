program benchmark
  implicit none
  integer,parameter :: seed = 86456
  integer :: n,i
  real x,y
  call srand(seed)
  n=__n__
  do i=1,n
     x = real(i)/real(n) * 2.
     y = 2.46 - 3.65*x**1.53 + __noise__*(rand()*2.-1.)
     write(*,*) x,y
  end do
end program benchmark

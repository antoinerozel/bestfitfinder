program benchmark
  implicit none
  integer,parameter :: seed = 86456
  integer :: n,i
  real x,y
  call srand(seed)
  n=30000
  do i=1,n
     x = real(i)/real(n) * 2.
     y = 2.46 - 3.65*x**1.53 + 0.*(rand()*2.-1.)
     write(*,*) x,y
  end do
end program benchmark

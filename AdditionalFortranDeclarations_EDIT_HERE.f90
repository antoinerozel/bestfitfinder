  ! any variable name can be input here (except variables which are already defined
  ! in bestfitfinder.sed.f90)
  ! This can help having intermediate parameters to write the
  ! regression formalism in a clean way
  real    :: type_name_real
  integer :: type_name_integer
  logical :: type_name_logical

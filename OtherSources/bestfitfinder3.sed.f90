           if (Stuff2FitContainsZEROS == 2) then
              misfit = misfit + (regression-stuff2fit(i))**2 /imaxreal
           else if (Stuff2FitContainsZEROS == 1) then
              misfit = misfit + ((regression-stuff2fit(i))/AverageValueToFit)**2 /imaxreal
           else
              misfit = misfit + ((regression-stuff2fit(i))/stuff2fit(i))**2 /imaxreal
           endif
           !endif
        enddo
        if (Stuff2FitContainsZEROS == 2) then
           misfit = sqrt(misfit)
        else
           misfit = 100.0 * sqrt(misfit)
        endif
        if (misfit.lt.bestmisfit) then
!__bloc2__
           bestmisfit = misfit
           write(*,*)bestmisfit!__blocbest__
        endif
        !
!__loop_closing__
        !
        wrongbound = 0
        if (a0best.eq.a0min) then
           write(*,*)"Bad boundary"
           a0min = a0min - (a0max-a0min)/2.0
           wrongbound = 1
!__bloc3__
        endif
        !
        if (a0best.eq.a0max) then
           write(*,*)"Bad boundary"
           a0min = a0max + (a0max-a0min)/2.0
           wrongbound = 1
!__bloc4__
        endif
        !
        if (wrongbound.eq.0) then
           write(*,*)"successful, increasing resolution"
           ! make the window smaller around the new solution
!__bloc5__
           !
           ires = ires + 1
        endif
     enddo
     write(118,*)bestmisfit!__blocbest__
  else
!!$     a0best = 8.6904861E-02
!!$     a1best = -9.2719623E-05
!!$     a2best = 9.6366666E-02
!!$     a3best = 2.1131273E-02
!!$     a4best = 4.8406520E-08
  endif
  endif ! end of forcefit=false
  ! plotting fit
  misfitfinal=0.0
  do i=1,imax
!__bloc6__

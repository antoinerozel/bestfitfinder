program modifplot
  implicit none
  integer, parameter :: imax = 52!,imaxno0 = 117
  real,dimension(1:imax)::ra
real,dimension(1:imax)::Ti
real,dimension(1:imax)::l
real,dimension(1:imax)::DT
real,dimension(1:imax)::m
real,dimension(1:imax)::Fe
real,dimension(1:imax)::Mg
real,dimension(1:imax)::Fem
real,dimension(1:imax)::Rp
real,dimension(1:imax)::Rc
real,dimension(1:imax)::g
real,dimension(1:imax)::rho
real,dimension(1:imax)::d
real,dimension(1:imax)::C
real,dimension(1:imax)::p
real,dimension(1:imax)::C2
real,dimension(1:imax)::Rck
!__0__
  !real,dimension (1:imax) :: __param__
  real,dimension (1:imax) :: stuff2fit
  !__1__
  real :: eta0
  real :: misfit,bestmisfit,regression
  real :: maxstepsreal,imaxreal
  real :: misfitfinal
  real :: HalfWindowSD,deltaSD,StandardDeviation,LocalMisfit
  real :: AverageValueToFit,Stuff2FitContainsZEROS
  !__SD_Declarations__
  integer :: i,j,label
  integer :: i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10
  integer :: maxsteps,ino0,ires
  integer :: wrongbound,lookforbest
  integer :: window_too_small,step_too_large,define_step,NstepsSD
  integer :: nit
  logical :: loop_ex
  !
  lookforbest = 1
  AverageValueToFit = 0.0
  Stuff2FitContainsZEROS = __Stuff2FitContainsZEROS__
  !
  open (unit=11,file="DataMatrix")
  ino0=1
  do i=1,imax
     read(11,*)ra(i),Ti(i),l(i),DT(i),m(i),Fe(i),Mg(i),Fem(i),Rp(i),Rc(i),g(i),rho(i),d(i),C(i),p(i),C2(i),Rck(i)
     stuff2fit(i) = C2(i)
     AverageValueToFit = AverageValueToFit + stuff2fit(i)
  enddo
  close (11)
  
end program modifplot

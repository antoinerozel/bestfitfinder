program stupidinversion
  implicit none
  ! This program takes tha data in AllData file and look for a
  ! I just stupidly try all combinaisons in given windows and
  ! takes the one that fits best (multiresolution now)
  ! eta0=1e19 Pa s
  !integer, parameter :: imax = 93,imaxno0 = 93
  integer, parameter :: imax = __imax__!,imaxno0 = 117
  !__0__
  !real,dimension (1:imax) :: __param__
  real,dimension (1:imax) :: stuff2fit
  !__1__
  real :: eta0
  real :: misfit,bestmisfit,regression
  real :: maxstepsreal,imaxreal
  real :: misfitfinal
  real :: HalfWindowSD,deltaSD,StandardDeviation,LocalMisfit
  real :: AverageValueToFit,Stuff2FitContainsZEROS
  real :: Probability,SumProbability

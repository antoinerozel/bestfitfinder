program represent
  implicit none
  real pmax,a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10
  real ra,Ti,l,DT,m,Fe,Mg,Fem,Rp,Rc,g,rho,d,C,p,C2,Rck
  real et,YS,Ex,MHF,CHF,RMC,Tm,Ti,Ct,ACt,C15,SlC,ER,ER2,M,Pn,IP,DzT
  integer i,imax,j
  !
  imax=100
  open(unit=11,file="VisuRefCase.dat")
  a0=1356.69702
  a1=48.6311226
  a2=0.415050596
  do j=1,3
  Ti=1800.0+ 200.0*(real(j)-2.0)
  do i=0,imax
     pmax=0.2
     p=real(i)/real(imax)*pmax
     !write(11,*)p,max(min(a0*p,a0*0.07*exp(-a1*(p-0.07))),0.0)
     !write(11,*)p,max(min(a0*p,a0*0.07*exp(-a1*(p-0.07)))+a2*(Ti-18e2)/200.0,0.0)
     !write(11,*)p,max(min(a0*p,a0*0.07*exp(-a1*(p-0.07))),0.0)*a2*(Ti)/18e2
     write(11,*)p,__eqvisu__
     __eqvisu2__
  enddo
  write(11,*)
  enddo
  close(11)
end program represent

  !__SD_Declarations__
  integer :: i,j,label
  integer :: i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10
  integer :: maxsteps,ino0,ires
  integer :: wrongbound,lookforbest
  integer :: window_too_small,step_too_large,define_step,NstepsSD
  integer :: nit
  integer :: numberofparams,forcefit
  logical :: loop_ex,showDetailsSD,LowBoundaryCannotBeFound,HighBoundaryCannotBeFound
  real    :: deltaProbability,ProbabilityBefore
  !
  showDetailsSD=__showDetailsSD__
  numberofparams = __np__
  forcefit = __ff__
  lookforbest = 1
  AverageValueToFit = 0.0
  Stuff2FitContainsZEROS = __Stuff2FitContainsZEROS__
  !
  open (unit=118,file="RESULT_chi_BestParams.dat")
  open (unit=119,file="RESULT_Regression_vs_Data.dat")
  open (unit=120,file="RESULT_StandardDevs.dat")
  open (unit=11,file="__matrixfilename__")
  ino0=1
  do i=1,imax
     read(11,*)__readin__
     stuff2fit(i) = __param2fit__
     AverageValueToFit = AverageValueToFit + stuff2fit(i)
  enddo
  close (11)
  AverageValueToFit = AverageValueToFit / real(imax)
  !
  !__minmaxdeclaration__
  !
  eta0 = 1e19
  !
  if (forcefit.eq.0) then
  if (lookforbest.eq.1) then
     maxsteps = __maxsteps__
     maxstepsreal = real(maxsteps)
     imaxreal = real(imax)
     bestmisfit = 1e30
     ires = 0
     do while (ires.lt.__ires__)
!__bloc1__
!__loop_start__
        misfit = 0.0
        do i=1,imax
           !if (E(i).ne.0.0)then

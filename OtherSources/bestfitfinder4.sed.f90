     !
     write(119,'(7e14.6)')regression,stuff2fit(i)!,E(i),eta(i),DT(i),age(i),W(i)
     
!!$     if (Stuff2FitContainsZEROS == 1) then
!!$        misfitfinal = misfitfinal + ((regression-stuff2fit(i))/AverageValueToFit)**2/real(imax)
!!$     else
!!$        misfitfinal = misfitfinal + ((regression-stuff2fit(i))/stuff2fit(i))**2/real(imax)
!!$     endif
     if (Stuff2FitContainsZEROS == 2) then
        misfitfinal = misfitfinal + (regression-stuff2fit(i))**2 /imaxreal
     else if (Stuff2FitContainsZEROS == 1) then
        misfitfinal = misfitfinal + ((regression-stuff2fit(i))/AverageValueToFit)**2 /imaxreal
     else
        misfitfinal = misfitfinal + ((regression-stuff2fit(i))/stuff2fit(i))**2 /imaxreal
     endif
     
     ! Paste here the plot restrictions generated by Generator_for_PLOT_RESTRICTION.sh
     !__PLOT_RESTRICTION__
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/TC_3000",position="append")
!__PLOT_RESTRICTION__if(TC(i).eq.3000.)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/TC_4000",position="append")
!__PLOT_RESTRICTION__if(TC(i).eq.4000.)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_0",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.0)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_11",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.11.025)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_22",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.22.05)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Ra_1e6",position="append")
!__PLOT_RESTRICTION__if(Ra(i).eq.1e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Ra_5e6",position="append")
!__PLOT_RESTRICTION__if(Ra(i).eq.5e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Ra_1e7",position="append")
!__PLOT_RESTRICTION__if(Ra(i).eq.1e7)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_0_Ra_1e6",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.0.and.Ra(i).eq.1e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_0_Ra_5e6",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.0.and.Ra(i).eq.5e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_0_Ra_1e7",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.0.and.Ra(i).eq.1e7)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_11_Ra_1e6",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.11.025.and.Ra(i).eq.1e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_11_Ra_5e6",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.11.025.and.Ra(i).eq.5e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_11_Ra_1e7",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.11.025.and.Ra(i).eq.1e7)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_22_Ra_1e6",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.22.05.and.Ra(i).eq.1e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_22_Ra_5e6",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.22.05.and.Ra(i).eq.5e6)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
!__PLOT_RESTRICTION__open(unit=11,file="Outputs/Rh_22_Ra_1e7",position="append")
!__PLOT_RESTRICTION__if(Rh(i).eq.22.05.and.Ra(i).eq.1e7)then
!__PLOT_RESTRICTION__write(11,*)stuff2fit(i),regression
!__PLOT_RESTRICTION__endif
!__PLOT_RESTRICTION__close(11)
     
     !
     !if (W(i) .eq. 4500.0) then
     !   write(120,'(7e14.6)')regression,stuff2fit(i)
     !elseif (W(i) .eq. 9000.0) then
     !   write(121,'(7e14.6)')regression,stuff2fit(i)
     !else
     !   write(122,'(7e14.6)')regression,stuff2fit(i)
     !endif
     !
  enddo
  if (Stuff2FitContainsZEROS == 2) then
     misfitfinal = sqrt(misfitfinal)
     write(*,*) "Average misfit: ",misfitfinal
  else
     misfitfinal = 100.0 * sqrt(misfitfinal)
     write(*,*) "Average misfit (%): ",misfitfinal
  endif
!!$  misfitfinal = 100.0*sqrt(misfitfinal)
  !open (11,file="brut/beta__beta___gamma__gamma__")
  !write(11,*)a0,__beta__,__gamma__,misfitfinal
  !close(11)
  !
  ! Computation of the standard deviation
